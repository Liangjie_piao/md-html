const {ipcRenderer} = require('electron');
const $ = require("jquery");
const fs = require('fs');
const marked = require('marked');
const cheerio = require("cheerio");

data = {
    changeUrl:null,
    mdArray:[],
    domHtmlUrl:null,
    domHtmlMB:"body"
}

/**
 * 操控----------------------------------------------
 */
$(()=>{
    //选择项目生成地址
    $(".Box .dz").click(()=>{
        openWindow("dz");
    });
    //选择md文件
    $(".Box .wj").click(()=>{
        openWindow("md");
    });
    //选择模板
    $(".Box .mb").click(()=>{
        openWindow("html");
    });
    //生成
    $(".Box ul li").click(()=>{
        //项目地址
        if(data.changeUrl){
                //md文档
            if(data.mdArray.length>0){
                //模板地址
                if(data.domHtmlUrl){
                    mdForHtml((res)=>{
                        //打开模板
                        console.log(data.domHtmlUrl);
                        openFile(data.domHtmlUrl[0],(rest)=>{
                            // console.log("模板",rest);
                            console.log("输入",res);
                            var L$ = cheerio.load(rest);
                            //替换模板中的内容
                            L$(data.domHtmlMB).html(res.html);
                            var newHtml = L$("html").html();
                            // console.log("合成",newHtml);
                            var addData = {
                                fileName:res.style.name,
                                fileStyle:"html",
                                fileUrl:data.changeUrl,
                                fileData:newHtml
                            }
                            //创建文件
                            reHtml(addData);
                        });
                    });
                }else{
                    //不套用模板
                    mdForHtml((res)=>{
                        var addData = {
                            fileName:res.style.name,
                            fileStyle:"html",
                            fileUrl:data.changeUrl,
                            fileData:res.html
                        }
                        //创建文件
                        reHtml(addData);
                    });
                }
            }else{
                erroBox("文件错误","请选择需要转译的md文档");
            }
        }else{
            erroBox("地址错误","请选择项目生成地址");
        }
    });
})
/**
 * 逻辑----------------------------------------------------
 */
//启动选择窗口
var openWindow = (getStyle)=>{
    var sendData = {}
    if(getStyle == "md"){
        sendData = {
            url:"index",//通讯接口
            style:"file",//通讯属性（file:文件夹窗口）
            send:"md",//请求内容类型（md:md文档，dz:项目保存地址，html:模板地址）
            properties:["openFile","multiSelections"],//选择文件（openDirectory:文件夹地址）
            filters:[{name:"Text",extensions:["md"]}]//文件类型
        }
    }else if(getStyle == "dz"){
        sendData = {
            url:"index",//通讯接口
            style:"file",//通讯属性（file:文件夹窗口）
            send:"dz",//请求内容类型（md:md文档，dz:项目保存地址，html:模板地址）
            properties:["openDirectory"],//选择文件（openDirectory:文件夹地址）
            filters:[{name:"All",extensions:["*"]}]//文件类型
        }
    }else if(getStyle == "html"){
        sendData = {
            url:"index",//通讯接口
            style:"file",//通讯属性（file:文件夹窗口）
            send:"html",//请求内容类型（md:md文档，dz:项目保存地址，html:模板地址）
            properties:["openFile"],//选择文件（openDirectory:文件夹地址）
            filters:[{name:"Text",extensions:["html"]}]//文件类型
        }
    }
    ipcRenderer.send("main",sendData);
};
/**
 * 读取文件
 * @param getUrl    string (打开文件路径【index.html】)
 * @param Fun       function (异步读取到数据后的执行方法)
 * @param PD        boolean  (true:直接带出字符串 false:marked处理后字符串)
 */
var openFile = (getUrl,Fun,PD)=>{
    fs.stat(getUrl,function(errs,fds){
        if(errs){
            console.error(errs);
        }
        console.log("预读文件信息",fds);
        fs.open(getUrl,'r+', function(err, fd){
            var buf = new Buffer.alloc(fds.size);
            fs.read(fd, buf, 0, buf.length, 0, function(err, bytes){
                if(err){
                    console.error(err);
                }

                if(bytes>0){
                    if(PD){
                        var FunDataOn = buf.toString();
                    }else{
                        var FunDataOn = marked(buf.toString());
                    }

                    //var FunData = JSON.parse(FunDataOn);
                    Fun(FunDataOn);
                }
            })
        });
    })
};
/**
 * md文件转换
 * @param Fun
 * outData = {
 *       html: string (读取的文件字符内容)
 *       style:obj  {
 *                      head:string (路径)
 *                      name:string （文件名）
 *                      style:string (文件类型)
 *                      url:string   (文件绝对路径)
 *                  }（读取文件的基本信息）
 * }
 */
var mdForHtml = (Fun)=>{
    var thisUrl = data.mdArray;
    var thisUrlL = thisUrl.length;
    for(var i = 0; i < thisUrlL; i++){
        var thisFUrl = thisUrl[i].url;
        var thisF = thisUrl[i];
        OF(thisFUrl,thisF);
    }
    function OF(thisFUrl,thisF){
        openFile(thisFUrl,(oRes)=>{
            //console.log("md文件内容",oRes);
            Fun({
                html:oRes,
                style:thisF
            });
        });
    }
};
/**
 * 生成html
 * @param getData  = {
 *                         fileName: string (新建文件名),
 *                         fileStyle:string (新建文件类型),
 *                         fileUrl:string   (生成路径),
 *                         fileData:string  (文件内容)
 *                      }
 */
var reHtml = (getData)=>{
    var fsUrl = getData.fileUrl + '\\' + getData.fileName + '.' + getData.fileStyle;
    fs.writeFile(fsUrl,getData.fileData,function(err){
        if(err){
            erroBox("生成文件错误",err);
        }
        console.log("文件生成成功",fsUrl);
    });
}
/**
 *md文件属性
 * @param getArg         string  ()
 * @returns {Array}      [{
 *                            url:   string (文件路径)
 *                            head:  string (所在文件夹位置)
 *                            style: string (文件类型)
 *                            name:  string (文件名)
 *                         }]
 */
var mdStyle = (getArg)=>{
    var outData = [];
    for(var i in getArg){
        var thisD = getArg[i];
        var thisN = thisD.split("\\");
        var thisNL = thisN.length-1;
        var thisNa = thisN[thisNL].split(".");
        var thisH = thisD.replace(thisN[thisNL],"");
        outData[i] = {
            url:thisD,
            head:thisH,
            style:thisNa[1],
            name:thisNa[0]
        }
    }
    return outData;
}
/**
 * 编换前端样式
 * @param Dom   string (JQ $ )
 * @param Text  string  (前端显示内容)
 */
var changeDiv = (Dom,Text)=>{
    $(Dom).html("");
    var fL = Text.length;
    for(var i =0 ;i < fL; i++){
        console.log(Text);
        var TA = Text[i].split('\\');
        var TAL = TA.length -1;
        if(fL>1){
            if(i<fL-1){
                $(Dom).append(TA[TAL] + "/");
            }else{
                $(Dom).append(TA[TAL]);
            }
        }else{
            $(Dom).append(TA[TAL]);
        }

    }
}
/**
 *错误弹窗
 * @param Etitle     弹窗标题
 * @param Econtent   说明内容
 */
var erroBox = (Etitle,Econtent)=>{
    ipcRenderer.send("main",{
        style:"err",
        title:Etitle,
        content:Econtent
    });
}
/**
 * 通讯------------------------------------------------------
 */
ipcRenderer.on("index",(event,arg)=>{
    console.log("ipc通讯",arg);
    //
    if(arg.ipc.send == "md"){//选择md文件
        var getData = {
            url:arg.res,
        }
        var FD = arg.res;
        data.mdArray = mdStyle(FD);
        changeDiv(".Box .wj div",FD);
        //mdForHtml(getData);
    }else if(arg.ipc.send == "html"){//选择模板
        data.domHtmlUrl = arg.res;
        changeDiv(".Box .mb div",arg.res);
    }else if(arg.ipc.send == "dz"){//选择项目地址
        data.changeUrl = arg.res;
        changeDiv(".Box .dz div",arg.res);
    }
});
